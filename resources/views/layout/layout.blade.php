<!DOCTYPE html>
<html lang="pt-BR" class='h-100'>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin</title>
    <link rel="stylesheet" href="{{  asset('css/app.css') }}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.13.0/css/all.css" integrity="sha384-Bfad6CLCknfcloXFOyFnlgtENryhrpZCe29RTifKEixXQZ38WheV+i/6YWSzkz3V" crossorigin="anonymous">
</head>
<body class='d-flex flex-column h-100'>
    <header class="header">

        <nav class="navbar navbar-expand-md navbar-dark fixed-top color-header-linear">
            <div class="row ml-2">
                <img src="{{ asset('img/logoBluemaxx.png') }}" alt="Logo Bluemaxx" width="230px">
            </div>
            <div class="row ml-auto mr-2">
                <img src="{{ asset('img/logoBerlamino.png') }}" alt="Logo Bluemaxx" width="120px">
            </div>
        </nav>
        <nav class="navbar navbar-expand-md navbar-dark fixed-top color-header-linear" style="top:88px; border-top: solid 1px #ffffff2e;">
            <button class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link font-weight-bold white" href="#" style="font-size: 16px; color:#fff">Relatórios</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link font-weight-bold white" href="#" style="font-size: 16px; color:#fff">Mudar Senha</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link font-weight-bold white" href="#" style="font-size: 16px; color:#fff">Sair</a>
                    </li>
                </ul>
            </div>
        </nav>
    </header>

    <main role="main" class="mt-5 mb-5">

        @yield('content')

    </main>
    <footer class="footer mt-auto py-3 color-footer-linear">
        <div class="container">
          <span class="text-muted"></span>
        </div>
    </footer>

    <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>

</body>
</html>