<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Guarupass - Admin</title>

        <link rel="stylesheet" href="{{  asset('css/app.css') }}">

    </head>
    <body class="text-center" id="section-login">
        <form class="form-signin" method="POST" action="{{ route('login') }}">
            @csrf
            <img id="logo-login" class="mb-4" src="{{ asset('img/logo_guarupass.png') }}" alt="">
            {{-- <h1 class="h3 mb-3 font-weight-normal">Please sign in</h1> --}}
            <label for="login" class="sr-only">Login</label>
                <input type="login" id="inputLogin" name='login' class="form-control mt-3 mb-3 border border-primary rounded-pill" placeholder="Login" required="true" autofocus="">
            <label for="inputPassword" class="sr-only">Senha</label>
                <input type="password" id="inputPassword" name='password' class="form-control mt-4 border border-primary rounded-pill" placeholder="Senha" required="true">
            <div class="row m-2">
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="lembrar">
                    <label class="custom-control-label" for="lembrar"> Lembrar</label>
                </div>
                <div class="checkbox mb-3 col text-right">
                    <label>
                        <a href="#"> Esqueceu a Senha ?</a>
                    </label>
                </div>
            </div>
            <input id='input-submit' class="btn btn-lg border border-primary rounded-pill d-block mr-auto ml-auto mt-4" type="submit" value="Enviar">
        </form>


        <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
    </body>
</html>