@extends('layout/layout')

@section('content')
    <div class="container mb-5 mt-5 text-center">
        <i class="fas fa-sign-out-alt fa-5x text-danger"></i>
        <h3 class="mt-4">Você foi desconectado da plataforma !</h3>
        <p>Clique no botão abaixo para se logar novamente</p>
        <button type="button" class="btn btn-lg btn-primary">Login</button>
    </div>
@endsection