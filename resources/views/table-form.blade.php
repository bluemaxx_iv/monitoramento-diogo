@extends('layout/layout')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-7 mb-4 mt-5 ml-1 mr-1 order-2 rounded-lg bg-white box-shadow shadow-lg p-3">
                <div class="mt-3 mb-3">
                    <nav class="bg-white navbar navbar-white p-0">
                        <form class="form-inline">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1"><i class="fas fa-search"></i></span>
                                </div>
                                <input type="text" class="form-control" placeholder="Pesquisa" aria-label="Username" aria-describedby="basic-addon1"/>
                            </div>
                        </form>
                    </nav>
                </div>
                <div class="p-0 rounded-lg table-responsive " style="max-height: 400px;">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Nome</th>
                                <th scope="col">CPF</th>
                                <th scope="col">Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>Teste</td>
                                <td>123.634.235-55</td>
                                <td><i class="fas fa-check ml-2 mr-2"></i><i class="fas fa-times ml-2 mr-2"></i><i class="far fa-trash-alt ml-2 mr-2"></i></td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>Teste</td>
                                <td>345.432.456-87</td>
                                <td><i class="fas fa-check ml-2 mr-2"></i><i class="fas fa-times ml-2 mr-2"></i><i class="far fa-trash-alt ml-2 mr-2"></i></td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>Teste</td>
                                <td>345s.543.542-45</td>
                                <td><i class="fas fa-check ml-2 mr-2"></i><i class="fas fa-times ml-2 mr-2"></i><i class="far fa-trash-alt ml-2 mr-2"></i></td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td>Teste</td>
                                <td>345.546.765-78</td>
                                <td><i class="fas fa-check ml-2 mr-2"></i><i class="fas fa-times ml-2 mr-2"></i><i class="far fa-trash-alt ml-2 mr-2"></i></td>
                            </tr>
                            <tr>
                                <td>5</td>
                                <td>Teste</td>
                                <td>867.576.458.-67</td>
                                <td><i class="fas fa-check ml-2 mr-2"></i><i class="fas fa-times ml-2 mr-2"></i><i class="far fa-trash-alt ml-2 mr-2"></i></td>
                            </tr>
                            <tr>
                                <td>5</td>
                                <td>Teste</td>
                                <td>867.576.458.-67</td>
                                <td><i class="fas fa-check ml-2 mr-2"></i><i class="fas fa-times ml-2 mr-2"></i><i class="far fa-trash-alt ml-2 mr-2"></i></td>
                            </tr>
                            <tr>
                                <td>5</td>
                                <td>Teste</td>
                                <td>867.576.458.-67</td>
                                <td><i class="fas fa-check ml-2 mr-2"></i><i class="fas fa-times ml-2 mr-2"></i><i class="far fa-trash-alt ml-2 mr-2"></i></td>
                            </tr>
                            <tr>
                                <td>5</td>
                                <td>Teste</td>
                                <td>867.576.458.-67</td>
                                <td><i class="fas fa-check ml-2 mr-2"></i><i class="fas fa-times ml-2 mr-2"></i><i class="far fa-trash-alt ml-2 mr-2"></i></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="mt-3">
                    <nav aria-label="Page navigation example">
                        <ul class="pagination">
                            <li class="page-item">
                                <a class="page-link" href="#">Anterior</a>
                            </li>
                            <li class="page-item">
                                <a class="page-link" href="#">1</a>
                            </li>
                            <li class="page-item">
                                <a class="page-link" href="#">2</a>
                            </li>
                            <li class="page-item">
                                <a class="page-link" href="#">3</a>
                            </li>
                            <li class="page-item">
                                <a class="page-link" href="#">Próximo</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="col-md-4 mt-5 mb-4 ml-1 mr-1 order-md-1 box-shadow shadow-lg p-5 rounded-lg bg-white">
                <form class="needs-validation" novalidate>
                    <div class="mb-4">
                        <label for="email">Email 
                            <span class="text-muted">(Optional)</span>
                        </label>
                        <input type="email" class="form-control" id="email" placeholder="you@example.com">
                        <div class="invalid-feedback">
                            Please enter a valid email address for shipping updates.
                        </div>
                    </div>
                    <div class="mb-4">
                        <label for="address">Address</label>
                        <input type="text" class="form-control" id="address" placeholder="1234 Main St" required>
                        <div class="invalid-feedback">
                            Please enter your shipping address.
                        </div>
                    </div>
                    <div class="mb-4">
                        <label for="address2">Address 2 
                            <span class="text-muted">(Optional)</span>
                        </label>
                        <input type="text" class="form-control" id="address2" placeholder="Apartment or suite">
                    </div>
                    <div class="row">
                        <div class="col-md-5 mb-3">
                            <label for="country">Country</label>
                            <select class="custom-select d-block w-100" id="country" required>
                                <option value="">Choose...</option>
                                <option>United States</option>
                            </select>
                            <div class="invalid-feedback">
                                Please select a valid country.
                            </div>
                        </div>
                        <div class="col-md-4 mb-3">
                            <label for="state">State</label>
                            <select class="custom-select d-block w-100" id="state" required>
                                <option value="">Choose...</option>
                                <option>California</option>
                            </select>
                            <div class="invalid-feedback">
                                Please provide a valid state.
                            </div>
                        </div>
                        <div class="col-md-3 mb-3">
                            <label for="zip">Zip</label>
                            <input type="text" class="form-control" id="zip" placeholder="" required>
                            <div class="invalid-feedback">
                                Zip code required.
                            </div>
                        </div>
                    </div>
                    <hr class="mb-4">
                    <button class="btn btn-primary btn-lg btn-block" type="submit">Submit</button>
                </form>
            </div>
        </div>
    </div>
@endsection