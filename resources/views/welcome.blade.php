@extends('layout/layout')

@section('content')

    <div class="container">
        <div class="row m-0">
            <div class="box-shadow col-md-12 mb-4 p-0 shadow-lg">
                <form action="">
                    <div class="border-light card mb-3">
                        <div class="card-header">Filtros</div>
                        <div class="card-body">
                            <div class="input-group mb-3 filtros-monitoramento">
                                <div class="input-group-prepend">
                                    <label class="input-group-text" for="inputGroupSelect01"><i class="fa fa-handshake"></i>  &nbsp;Gestão</label>
                                </div>
                                <select class="custom-select" id="inputGroupSelect01">
                                    <option selected="">Choose...</option>
                                    <option value="1">One</option>
                                    <option value="2">Two</option>
                                    <option value="3">Three</option>
                                </select>
                            </div>
                            <div class="input-group mb-3 filtros-monitoramento">
                                <div class="input-group-prepend">
                                    <label class="input-group-text" for="inputGroupSelect01"><i class="fa fa-map "></i> &nbsp;Garagem</label>
                                </div>
                                <select class="custom-select" id="inputGroupSelect01">
                                    <option selected="">Choose...</option>
                                    <option value="1">One</option>
                                    <option value="2">Two</option>
                                    <option value="3">Three</option>
                                </select>
                            </div>
                            <div class="input-group mb-3 filtros-monitoramento">
                                <div class="input-group-prepend">
                                    <label class="input-group-text" for="inputGroupSelect01"><i class="fa fa-bus "></i> &nbsp;Tipo</label>
                                </div>
                                <select class="custom-select" id="inputGroupSelect01">
                                    <option selected="">Choose...</option>
                                    <option value="1">One</option>
                                    <option value="2">Two</option>
                                    <option value="3">Three</option>
                                </select>
                            </div>
                            <div class="input-group mb-3 filtros-monitoramento">
                                <div class="input-group-prepend">
                                    <label class="input-group-text" for="inputGroupSelect01"><i class="fa fa-toggle-on "></i> &nbsp;Status</label>
                                </div>
                                <select class="custom-select" id="inputGroupSelect01">
                                    <option selected="">Choose...</option>
                                    <option value="1">One</option>
                                    <option value="2">Two</option>
                                    <option value="3">Three</option>
                                </select>
                            </div>
                            <div class="input-group mb-3 filtros-monitoramento">
                                <div class="input-group-prepend">
                                    <label class="input-group-text" for="inputGroupSelect01"><i class="fa fa-users "></i> &nbsp;Cliente</label>
                                </div>
                                <select class="custom-select" id="inputGroupSelect01">
                                    <option selected="">Choose...</option>
                                    <option value="1">One</option>
                                    <option value="2">Two</option>
                                    <option value="3">Three</option>
                                </select>
                            </div>
                            <div class="input-group mb-3 filtros-monitoramento">
                                <div class="input-group-prepend">
                                    <label class="input-group-text" for="inputGroupSelect01"><i class="fa fa-chart-pie "></i> &nbsp;Segmento</label>
                                </div>
                                <select class="custom-select" id="inputGroupSelect01">
                                    <option selected="">Choose...</option>
                                    <option value="1">One</option>
                                    <option value="2">Two</option>
                                    <option value="3">Three</option>
                                </select>
                            </div>
                            <div class="input-group mb-3 filtros-monitoramento">
                                <div class="input-group-prepend">
                                    <label class="input-group-text" for="inputGroupSelect01"><i class="fa fa-briefcase "></i> &nbsp;Empresa</label>
                                </div>
                                <select class="custom-select" id="inputGroupSelect01">
                                    <option selected="">Choose...</option>
                                    <option value="1">One</option>
                                    <option value="2">Two</option>
                                    <option value="3">Three</option>
                                </select>
                            </div>
                            <div class="input-group-append d-none d-inline-block ml-3 mr-3" style="">
                                <button type="submit" class="btn btn-primary text-white" style=""><i class="fa fa-filter  text-white"></i> Filtrar Dados </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="row m-0">
            <div class="box-shadow col-md-12 mb-4 p-0 shadow-lg">
                <div class="border-light card">
                    <div class="card-header">Status Atual</div>
                    <div class="card-body">
                        <div class="button-bus bg-success">
                            <i class="fas fa-bus icon-status"></i>
                            <p class="text-bus">0241 Conectados</p>
                        </div>
                        <div class="button-bus bg-warning text-dark">
                            <i class="fas fa-bus icon-status"></i>
                            <p class="text-bus">0241 Em Atenção</p>
                        </div>
                        <div class="button-bus bg-danger">
                            <i class="fas fa-bus icon-status"></i>
                            <p class="text-bus">0241 Em Alerta</p>
                        </div>
                        <div class="button-bus bg-info">
                            <i class="fas fa-bus icon-status"></i>
                            <p class="text-bus">0241 Em Manutenção</p>
                        </div>
                        <div class="button-bus bg-secondary">
                            <i class="fas fa-bus icon-status"></i>
                            <p class="text-bus">0241 Inativos</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 mb-4 mt-3 order-2 rounded-lg bg-white box-shadow shadow-lg p-3">
            <div class="mt-3 mb-3">
                <nav class="bg-white navbar navbar-white p-0">
                    <form class="form-inline">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1"><i class="fas fa-search"></i></span>
                            </div>
                            <input type="text" class="form-control" placeholder="Pesquisa" aria-label="Username" aria-describedby="basic-addon1"/>
                        </div>
                    </form>
                </nav>
            </div>
            <div class="p-0 rounded-lg table-responsive">
                <table class="table table-hover" id="table-monitoramento">
                    <thead>
                        <tr>
                            <th scope="col">Gestão</th>
                            <th scope="col">Empresa</th>
                            <th scope="col">Cliente</th>
                            <th scope="col">Garagem</th>
                            <th scope="col">Tipo</th>
                            <th scope="col">Segmento</th>
                            <th scope="col">Prefixo</th>
                            <th scope="col">Patrimônio</th>
                            <th scope="col">Identity</th>
                            <th scope="col">Status</th>
                            <th scope="col">Ação</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>CAMPO DOS OUROS</td>
                            <td>ATUAL</td>
                            <td>ATUAL</td>
                            <td>CAMPO DOS OUROS</td>
                            <td>FRETADO</td>
                            <td>FRETADO</td>
                            <td>1125B</td>
                            <td>787</td>
                            <td>ATUAL-787-Fretamento-1125B</td>
                            <td>Conectado</td>
                            <td><i class="far fa-more ml-2 mr-2"></i></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="row">
                <div class="mt-3 col-md-6">
                    <nav aria-label="Page navigation example">
                        <ul class="pagination">
                            <li class="page-item">
                                <a class="page-link" href="#">Anterior</a>
                            </li>
                            <li class="page-item">
                                <a class="page-link" href="#">1</a>
                            </li>
                            <li class="page-item">
                                <a class="page-link" href="#">2</a>
                            </li>
                            <li class="page-item">
                                <a class="page-link" href="#">3</a>
                            </li>
                            <li class="page-item">
                                <a class="page-link" href="#">Próximo</a>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class='box-dropdown mt-3 col-md-6 text-right'>
                    <div class="btn-group">
                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Selecione</button>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="#">10</a>
                            <a class="dropdown-item" href="#">20</a>
                            <a class="dropdown-item" href="#">50</a>
                            <a class="dropdown-item" href="#">100</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection